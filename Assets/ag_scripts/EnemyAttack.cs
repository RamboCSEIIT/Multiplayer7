﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class EnemyAttack : MonoBehaviour {

    public int attackDamage = 50;
    public float timeBetweenAttacks = 0.5f;
    public bool playerInRange = false;
    float timer;
    public float Damage = 10.0f;
    public bool isenabled=true;

    EnemyHealth enemyHealth;
    PlayerHealth playerHealth;
    Animator anim;
    Animator playeranim;
    GameObject player;

    NavMeshAgent navMeshAgent;
    shootPlayer shootplayer;
    playerscript playerScript;
    
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player");
        enemyHealth = GetComponent<EnemyHealth>();
        playerHealth = player. GetComponent<PlayerHealth>();
        playeranim =player. GetComponent<Animator>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        shootplayer = player.GetComponent<shootPlayer>();

        playerScript = player.GetComponent<playerscript>();
    }
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject== player)
        {
            playerInRange = true;
        }
        
    }


    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == player)
        {
            playerInRange = false;
        }
       // Destroy(other.gameObject);
    }

    // Update is called once per frame
    void Update () {

        if (isenabled == false)
            return;
        timer += Time.deltaTime;
        if(timer>=timeBetweenAttacks && playerInRange && enemyHealth.currentHealth>=0)
        {
            Attack();
        }
        if(playerHealth.currentHealth<=0)
        {
           // playeranim.SetTrigger("Death");
           playerHealth.Death();
            isenabled = false;
            anim.SetTrigger("Dance");
          //  navMeshAgent.enabled = false;
            shootplayer.disable_shoot();
            playerScript.disable_movement();
        }
            
	}
    void Attack()
    {
        timer=0.0f;
        anim.SetTrigger("Attack");
        if(playerHealth.currentHealth>0)
        {
            playerHealth.TakeDamage(attackDamage);
        }

    }
}
