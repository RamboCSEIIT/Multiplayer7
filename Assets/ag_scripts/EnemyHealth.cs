﻿using UnityEngine;

using UnityEngine.AI;
public class EnemyHealth : MonoBehaviour {


    public float startingHealth =100.0f;
    public float currentHealth = 100.0f;
    bool isDead = false;
    Animator anim;
    NavMeshAgent agent;
    ParticleSystem hitparticle;
    // Use this for initialization
    void Start () {
        currentHealth = startingHealth;
       anim = GetComponent<Animator>();
       agent = GetComponent<NavMeshAgent>();
        hitparticle = GetComponent<ParticleSystem>();
      //  Death();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public void TakeDamage(int amount,Vector3 hitPoint)
    {
        if (isDead)
            return;
        hitparticle.transform.position = hitPoint;
        hitparticle.Play();
        currentHealth -= amount;
        if(currentHealth<=0)
        {
            Death();
        }

    }
    void Death()
    {
        anim.SetTrigger("Death");
        //  agent.enabled = false;
        agent.speed = 0;
        isDead = true;
        Destroy(gameObject, 2.0f);
    }
}
