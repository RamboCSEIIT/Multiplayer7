﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour {


    public float startingHealth = 100.0f;
    public float currentHealth = 100.0f;
    bool isDead = false;
    Animator anim;
     void Start()
    {
        currentHealth = startingHealth;
        anim = GetComponent<Animator>();
        //  Death();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void TakeDamage(int amount)
    {
        if (isDead)
            return;
         currentHealth -= amount;
        if (currentHealth <= 0)
        {
            Death();
        }

    }
   public  void Death()
    {

        if (isDead)
            return;
        anim.SetTrigger("Death");
        //  agent.enabled = false;
       
        isDead = true;
       // Destroy(gameObject, 2.0f);
    }

}
