﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraFollower : MonoBehaviour {
    [SerializeField]
    float Camera_smoothing = 5.0f;
    public GameObject target;
    Vector3 offset;
	// Use this for initialization
	void Start () {

       if(target!=null)
        offset = transform.position - target.transform.position;
	}
	
	// Update is called once per frame
	void Update () {

      if(target!=null)
        {
            Vector3 target_camera = target.transform.position + offset;
            transform.position = Vector3.Lerp(transform.position, target_camera, Camera_smoothing);


        }


    }
}
