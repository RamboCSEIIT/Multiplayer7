﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class enemy : MonoBehaviour {
[SerializeField]
    GameObject player;
    NavMeshAgent nav;
    [SerializeField]
    Material [] em;
    [SerializeField]
    int color_number = 0;

	// Use this for initialization
	void Start ()
    {
        player = GameObject.FindWithTag("Player");
        nav = GetComponent<NavMeshAgent>();
        color_number = Random.Range(0, em.Length);
        this.transform.GetChild(0).GetComponent<Renderer>().material = em[color_number];
            
    }
	
	// Update is called once per frame
	void Update () {

        if (nav.enabled == false)
            return;
        nav.SetDestination(player.transform.position);
		
	}
}
