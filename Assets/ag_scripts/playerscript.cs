﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnitySampleAssets.CrossPlatformInput;
using UnityEngine.Networking;


public class playerscript : NetworkBehaviour
{
    Rigidbody playerRigidbody;
    Vector3 movement;
    [SerializeField]
    float SPEED = 5.0f;
    [SerializeField]
    float ROT_SPEED = 100.0f;


    [SerializeField]
    Animator anim;
    bool isMoving = false;
    [SerializeField]
    int floorMask;
    [SerializeField]
    RaycastHit hit;
    [SerializeField]
    Ray CamRay;
    [SerializeField]
    Camera camera;
    [SerializeField]
    Ray player_ray;

    [SerializeField]
    RaycastHit player_hit;
    
    float RAY_LENGTH = 1000.0f;

    [SerializeField]
    float ANGULAR_VELOCITY=50.0f;

    public bool isenabled = true;

    public void disable_movement()
    {
        isenabled = false;
    }
    void Awake()
    {
        playerRigidbody = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        floorMask = LayerMask.GetMask("floor");
    }
    // Use this for initialization
    void Start()
    {
        this.transform.GetChild(2).transform.GetComponent<Renderer>().material.color = Color.Lerp(Color.white, Color.red, Random.Range(0.0f,1.0f ));
    }
    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {

        if (isenabled == false)
            return;
        float h = CrossPlatformInputManager.GetAxis("Horizontal");
        float v = CrossPlatformInputManager.GetAxis("Vertical");

        if(isLocalPlayer)
        {

            CmdMove(h, v);
            //  Debug.Log(h + "::" + v);
            if (!Mathf.Approximately(v, 0.0f) || !Mathf.Approximately(h, 0.0f))
            {
                isMoving = true;
            }
            else
            {
                isMoving = false;
            }

            CmdMakeAnimation();
            //  MouseLook();
        }




    }
 
    void CmdMove(float h, float v)
    {
         Vector3 m_EulerAngleVelocity;
        m_EulerAngleVelocity = new Vector3(0, ROT_SPEED, 0);

        Quaternion deltaRotation = Quaternion.Euler(m_EulerAngleVelocity * Time.deltaTime*h);

        playerRigidbody.MoveRotation(playerRigidbody.rotation * deltaRotation);

        movement = (transform.right * h + transform.forward * v);

        Debug.DrawRay(transform.position, movement * 100.0f, Color.magenta);
        //  Quaternion targetRotation = Quaternion.LookRotation(movement * 100.0f, transform.up);
        //   transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * 2.0f);


        //  Quaternion new_rotation = Quaternion.LookRotation();
        // Do something with the object that was hit by the raycast.
        //  playerRigidbody.MoveRotation(new_rotation);

      //  Quaternion deltaRotation = Quaternion.Euler(m_EulerAngleVelocity * Time.deltaTime);
        //m_Rigidbody.MoveRotation(m_Rigidbody.rotation * deltaRotation);


        //  movement = (transform.right * 0 + transform.forward * v)*SPEED*Time.deltaTime;
        //  var targetRotation = Quaternion.LookRotation(targetPoint - transform.position, transform.up);
        //  transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * 2.0f);

        ///  Quaternion deltaRotation = Quaternion.Euler(transform.up* ANGULAR_VELOCITY * Time.deltaTime*h);
        //  playerRigidbody.MoveRotation(playerRigidbody.rotation * deltaRotation);
        // movement.Set(h, 0.0f, v);
        //movement = movement.normalized * Time.deltaTime * SPEED;


         playerRigidbody.MovePosition(transform.position + movement);
    }
 
    void CmdMakeAnimation()
    {
        if(isMoving)
        {

//Debug.Log("Inside is moving 1");
            anim.SetFloat("isMoving", 1.0f);
        }
        else
        {
//Debug.Log("Inside is moving 2");

            anim.SetFloat("isMoving", 0.0f);
        }
    }
    void MouseLook()
    {
        CamRay = camera.ScreenPointToRay(Input.mousePosition);
       // Debug.DrawRay(CamRay.origin, CamRay.direction*1000, Color.red);

        if (Physics.Raycast(CamRay, out hit, RAY_LENGTH, floorMask ))
        {
            Transform objectHit = hit.transform;
          //  Vector3 Camera_to_Mouse = hit.point - transform.position;

            Vector3 Pointer_to_Mouse = hit.point - transform.position;
          //  Debug.Log("Player center::" + transform.position);
          //  Debug.DrawLine( center.position, hit.point,Color.green);
            Pointer_to_Mouse.y = 0;
            player_ray.origin = transform.position;
            player_ray.direction = -transform.up*1000.0f;
            if (Physics.Raycast(player_ray, out player_hit, RAY_LENGTH, floorMask))
            {
               // Debug.DrawLine(transform.position, hit.point, Color.magenta);

            }

            Quaternion new_rotation = Quaternion.LookRotation(Pointer_to_Mouse);
            // Do something with the object that was hit by the raycast.
            playerRigidbody.MoveRotation(new_rotation);
        }
        else
        {
           // Debug.Log("no hit");
        }


    }
}