﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

 

public class shootPlayer : NetworkBehaviour
{
    [SyncVar]
    public int Vaal = 45;
    [SyncVar]
    public int fire_client = 100;

    [SyncVar]
    public int fire_from_server = 100;


    RaycastHit shootHit;
    Ray shootRay;
   public  LineRenderer lasterLine;
    [SerializeField]
    int shootableMask;

    public Light gunLight;

    [SerializeField]
    float RAY_LENGTH = 400.0f;
  //  [SerializeField]
  //  GameObject LaserBeamOrigin;

    [SerializeField]
    GameObject GunTip;

    [SerializeField]
    int  BulletIndex;


    [SerializeField]
    float BULLET_SPEED;
    [SerializeField]
    float BULLET_LIFE;

    [SerializeField]
    float BULLET_POSADJUST;
    public  GameObject [] bullets;

    [SerializeField]
   public bool isShooting = false;
    [SerializeField]
    Animator anim;

    [SerializeField]
    int DAMAGE_COUNT = 10;
    public bool isenabled=true;
    private GameObject selectedProjectilePrefab;
    private GameObject currentPrefabObject;
    
    // Use this for initialization
    void Start ()
    {
        shootableMask = LayerMask.GetMask("enemy");
        lasterLine = GetComponentInChildren<LineRenderer>();
    //    LaserBeamOrigin = GameObject.FindGameObjectWithTag("LaserBeamOrigin");
     //   GunTip = GameObject.FindGameObjectWithTag("guntipF");
        anim = GetComponent<Animator>();
        lasterLine.enabled = false;
        gunLight.enabled = false;

        BulletIndex = Random.Range(0, bullets.Length);

        InitializeProjectile();
    }

    void InitializeProjectile()
    {

         
            selectedProjectilePrefab = bullets[BulletIndex];
    }


    // Update is called once per frame
    void Update ()
    {
        if (!isLocalPlayer)
            return;
        
            if (Input.GetKeyDown(KeyCode.Space) && isShooting == false && isenabled == true)
            {

                isShooting = true;
                //    anim.SetFloat("shoot", 1.0f);
                // Cmdshoot();

                CmdshootBullet();
           // CmdshootBulletCL();


                 Invoke("stopShooting",0.1f);
        }

       
 

        }


    

 
    [Command]
    void CmdshootBullet()
    {
        // Create the Bullet from the Bullet Prefab
   

               Invoke("Invoke_recoil_Shooting",  .2f);
        SpawnProjectile();
       // Destroy(bullet, 2.0f);

    }
    [Client]
    void CmdshootBulletCL()
    {
        // Create the Bullet from the Bullet Prefab


        Invoke("Invoke_recoil_Shooting", .2f);
        SpawnProjectileC();
        // Destroy(bullet, 2.0f);

    }
    void SpawnProjectileC()
    {
        currentPrefabObject = GameObject.Instantiate(selectedProjectilePrefab);
        currentPrefabObject.transform.position = GunTip.transform.position;
        currentPrefabObject.transform.rotation = transform.rotation;

        currentPrefabObject.transform.position = GunTip.transform.position + GunTip.transform.right * BULLET_POSADJUST;
        // Add velocity to the bullet
        currentPrefabObject.transform.GetComponent<Rigidbody>().velocity = GunTip.transform.right * BULLET_SPEED;
        currentPrefabObject.transform.rotation = GunTip.transform.rotation;
    //    NetworkClient.Spawn(currentPrefabObject);


    }

    void SpawnProjectile()
    {
        currentPrefabObject = GameObject.Instantiate(selectedProjectilePrefab);
        currentPrefabObject.transform.position = GunTip.transform.position;
        currentPrefabObject.transform.rotation = transform.rotation;

        currentPrefabObject.transform.position = GunTip.transform.position + GunTip.transform.right * BULLET_POSADJUST;
        // Add velocity to the bullet
        currentPrefabObject.transform.GetComponent<Rigidbody>().velocity = GunTip.transform.right * BULLET_SPEED;
        currentPrefabObject.transform.rotation = GunTip.transform.rotation;
         NetworkServer.Spawn(currentPrefabObject);
        

    }

    void Cmdshoot()
    {

     lasterLine.enabled = true;
     gunLight.enabled = true;


        // Debug.Log("Shoot 1");
        //https://docs.unity3d.com/ScriptReference/LineRenderer.SetPosition.html
        // lasterLine.enabled = true;
        /*
                shootRay.origin = LaserBeamOrigin.transform.position;
                shootRay.direction = transform.forward;
        */
        shootRay.origin = GunTip.transform.position;
        shootRay.direction = GunTip.transform.right;

        Debug.DrawRay(GunTip.transform.position, GunTip.transform.transform.right*1000,Color.red);
       

        //  shootray.direction = Vector3.Normalize(enemy_pos.position - transform.position);
        //  enemy = enemy_pos.position;

        // Debug.DrawRay(shootray.origin,shootray.direction*RAY_LENGTH,Color.green);

        if (Physics.Raycast(shootRay, out shootHit, RAY_LENGTH, shootableMask))
        {
            lasterLine.SetPosition(0, GunTip.transform.position);

            lasterLine.SetPosition(1, shootHit.point);
            Debug.Log("Enemy Hit");

           
            EnemyHealth enemyHealth = shootHit.collider.GetComponent<EnemyHealth>();
            if (enemyHealth != null)
            {
                enemyHealth.TakeDamage(DAMAGE_COUNT, shootHit.point);
            } 
        }
        else
        {
            lasterLine.SetPosition(0, GunTip.transform.position);

            lasterLine.SetPosition(1, GunTip.transform.position + RAY_LENGTH* GunTip.transform.right);

        }

        Invoke("Invoke_recoil_Shooting", 1.0f);

    }

    void Invoke_recoil_Shooting()
    {
        anim.SetFloat("shoot", 1.0f);
    }

    public void disable_shoot()
    {
        isenabled = false;
    }

    void Disable_server()
    {
        anim.SetFloat("shoot", 1.0f);
    }



}
