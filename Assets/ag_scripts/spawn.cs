﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawn : MonoBehaviour {
    public GameObject objectToSpawn;
    public float TimeWaitBetweenSpawns;
    public float Timer = 0.0f;

    // Use this for initialization
    void Start () {

        Timer = 0;
		
	}
	
	// Update is called once per frame
	void Update () {
        Timer += Time.deltaTime;
        GameObject player = null;
          player = GameObject.FindWithTag("Player");

        if (Timer > TimeWaitBetweenSpawns && player!=null)
        {
            Instantiate(objectToSpawn, transform.position, transform.rotation);
            Timer = 0.0f;
        }
	}
}
